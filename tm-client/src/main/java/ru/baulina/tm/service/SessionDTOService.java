package ru.baulina.tm.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.service.ISessionDTOService;
import ru.baulina.tm.endpoint.SessionDTO;

@Service
public class SessionDTOService implements ISessionDTOService {

    @Nullable
    private SessionDTO session = null;

    @Override
    public void setSession(@Nullable final SessionDTO session) {
        this.session = session;
    }

    @Nullable
    @Override
    public SessionDTO getSession() {
        return session;
    }

    @Nullable
    @Override
    public Long getUserId() {
        if (session == null) return null;
        return session.getUserId();
    }

}
