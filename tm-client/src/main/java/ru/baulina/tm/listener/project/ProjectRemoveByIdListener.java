package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectRemoveByIdListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT]");
        @Nullable final Long id;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER ID");
            id = TerminalUtil.nexLong();
        }
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.removeOneProjectById(session, id);
        System.out.println("[OK]");
        System.out.println();
    }

}
