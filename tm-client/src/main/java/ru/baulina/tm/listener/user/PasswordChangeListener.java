package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class PasswordChangeListener extends AbstractUserListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Change user's password.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@passwordChangeListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CHANGE_PASSWORD]");
        @NotNull final String passwordOld;
        @NotNull final String passwordNew;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER OLD PASSWORD:");
            passwordOld = TerminalUtil.nextLine();
            System.out.println("ENTER NEW PASSWORD:");
            passwordNew = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        userEndpoint.changeUserPassword(session, passwordOld, passwordNew, session.getUserId());
        System.out.println("[OK]");
        System.out.println();
    }

}
