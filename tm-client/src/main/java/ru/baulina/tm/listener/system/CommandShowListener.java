package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.api.service.ICommandService;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.List;

@Component
public class CommandShowListener extends AbstractListener {

    @Autowired
    private ICommandService commandService;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@commandShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        commandShow();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@commandShowListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        commandShow();
    }

    private void commandShow() {
        @NotNull final List<String> commands = commandService.getCommandsNames();
        System.out.println(Arrays.toString(commands.toArray()));
        System.out.println();
    }
}
