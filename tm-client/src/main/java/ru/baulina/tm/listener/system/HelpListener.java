package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

import java.util.List;

@Component
public class HelpListener extends AbstractListener {

    @Lazy
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[HELP]");
        listeners.stream().forEach(x->System.out.println(x));
        System.out.println("[OK]");
        System.out.println();
    }

}
