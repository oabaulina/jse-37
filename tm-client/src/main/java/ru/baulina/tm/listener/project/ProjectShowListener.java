package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.*;
import ru.baulina.tm.event.ConsoleEvent;

import java.util.List;

@Component
public class ProjectShowListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show task project.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[LIST PROJECT]");
        @Nullable final SessionDTO session = getSession();
        @Nullable final List<ProjectDTO> projects = projectEndpoint.findAllProjects(session);
        int index = 1;
        for (ProjectDTO project: projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
