package ru.baulina.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


public final class SignatureUtil {

    @Nullable
    public static String sign(@NotNull final Object value, @Nullable String salt, @Nullable Integer iteration){
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);

            return sign(json, salt, iteration);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String sign(@Nullable final String value, @Nullable String salt, @Nullable Integer iteration){
        if (value == null || salt == null || iteration == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = HashUtil.md5hashing(salt + result + salt);
        }
        return result;
    }
}
