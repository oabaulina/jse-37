package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @NotNull
    default
    List<Session> getListSession() {
        return findAll();
    };

    default
    void remove(@NotNull final Session session) {
        delete(session);
    };

    @NotNull
    List<Session> findAllByUserId(@NotNull final Long userId);

    default
    @NotNull
    List<Session> findByUserId(@NotNull final Long userId) {
        return findAllByUserId(userId);
    };

    @Nullable
    Session deleteByUserId(@NotNull final Long userId);

    default
    @Nullable
    void removeByUserId(@NotNull final Long userId) {
        deleteByUserId(userId);
    };

    default
    boolean contains(@NotNull final Long id) {
        return existsById(id);
    };

}
