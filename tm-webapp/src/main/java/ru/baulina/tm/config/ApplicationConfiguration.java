package ru.baulina.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.baulina.tm")
public class ApplicationConfiguration {

}
