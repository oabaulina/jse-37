package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;

import ru.baulina.tm.model.Project;
import ru.baulina.tm.model.Task;
import ru.baulina.tm.model.User;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @Nullable
    Task create(@Nullable User user,
                @Nullable String name
    );

    @Nullable
    Task create(
            @Nullable User user,
            @Nullable String name, @Nullable String description
    );

    @Nullable
    List<Task> findAll(@Nullable final Long userId);

    void clear(@Nullable final Long userId);

    @Nullable List<Task> findListTasks();

    @Nullable
    Task findOneById(
            @Nullable Long userId,
            @Nullable Long id
    );

    @Nullable
    Task findOneByName(
            @Nullable Long userId,
            @Nullable String name
    );

    void remove(@Nullable Task task);

    void removeOneById(
            @Nullable final Long userId,
            @Nullable Long id);

    void removeOneByName(
            @Nullable Long userId,
            @Nullable String name
    );

    void updateTaskById(
            @Nullable Long userId,
            @Nullable Long id,
            @Nullable String name, @Nullable String description
    );

}
