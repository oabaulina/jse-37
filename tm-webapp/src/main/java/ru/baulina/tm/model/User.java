package ru.baulina.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.enumerated.Role;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@XmlRootElement
@Table(name="user_table")
public class User extends AbstractModel implements Serializable {

    @NotNull
    @Column(unique = true, nullable = false)
    private String login;

    @NotNull
    @Column(name="password", nullable = false)
    private String passwordHash;

    @Nullable
    private String email;

    public User() {
    }

    public User(@NotNull String login) {
        this.login = login;
    }

    @Nullable
    @Column(name="first_name")
    private String firstName;

    @Nullable
    @Column(name="last_name")
    private String lastName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

    @NotNull
    @Column(name="locked")
    private Boolean locked = false;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

}
