package ru.baulina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.model.User;

import java.util.List;

@Repository
public interface IUserRepository extends IRepository<User> {

    @NotNull
    @Override
    List<User> findAll();

    @Nullable
    User findByLoginAndPasswordHash(@NotNull final String login, @NotNull final String password);

    default User findUser(@NotNull final String login, @NotNull final String password) {
        return findByLoginAndPasswordHash(login, password);
    }

    @Nullable
    User findByLogin(@NotNull final String login);

    void deleteByLogin(@NotNull final String login);

}
