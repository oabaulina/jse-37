package ru.baulina.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.baulina.tm.model.AbstractModel;

@Repository
public interface IRepository<E extends AbstractModel> extends JpaRepository<E, Long> {
}
