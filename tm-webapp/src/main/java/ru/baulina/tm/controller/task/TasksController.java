package ru.baulina.tm.controller.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.ITaskService;

import javax.servlet.http.HttpSession;

@Controller
public class TasksController {

    private final ITaskService taskService;

    @Autowired
    public TasksController(
            @NotNull final ITaskService taskService
    ) {
        this.taskService = taskService;
    }

    @GetMapping("/tasks")
    public ModelAndView index(@NotNull HttpSession session) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        return new ModelAndView("task/task-list", "tasks", taskService.findAll(userId));
    }

}
