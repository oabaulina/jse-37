package ru.baulina.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class RegisterController {

    private final IUserService userService;

    @Autowired
    public RegisterController(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String singIn() {
        return "register";
    }

    @PostMapping("/register")
    public ModelAndView postRegisterPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final String login = req.getParameter("login");
        @NotNull final String password = req.getParameter("psw");

        @Nullable final User isExistUser = userService.findByLogin(login);

        if (isExistUser != null) {
            resp.sendRedirect("/register");
            return new ModelAndView("/register","errorMessage","Wrong!!!");
        }

        @Nullable final User registryUser = userService.create(login, password);
        resp.sendRedirect("/login");
        return new ModelAndView("/","errorMessage","Registered Successfully!");
    }

}
