package ru.baulina.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class ExitController {

    @PostMapping("/exit")
    public ModelAndView postLogin(@NotNull HttpSession session) {
        session.invalidate();
        return new ModelAndView("redirect:/");
    }

}
