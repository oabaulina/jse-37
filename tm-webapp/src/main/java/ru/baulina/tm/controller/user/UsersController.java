package ru.baulina.tm.controller.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.IUserService;

@Controller
public class UsersController {

    private final IUserService userService;

    @Autowired
    public UsersController(
            @NotNull final IUserService userService
    ) {
        this.userService = userService;
    }

    @GetMapping("/users")
    public ModelAndView index() {
        return new ModelAndView("user/user-list", "users", userService.findAll());
    }

}
