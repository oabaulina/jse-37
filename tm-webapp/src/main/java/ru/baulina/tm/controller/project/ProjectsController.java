package ru.baulina.tm.controller.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.repository.IProjectRepository;

import javax.servlet.http.HttpSession;

@Controller
public class ProjectsController {

    private final IProjectService projectService;

    @Autowired
    public ProjectsController(
            @NotNull final IProjectService projectService
    ) {
        this.projectService = projectService;
    }

    @GetMapping("/projects")
    public ModelAndView index(@NotNull HttpSession session) {
        @NotNull final Long userId = (Long) session.getAttribute("userId");
        return new ModelAndView("project/project-list", "projects", projectService.findAll(userId));
    }

}
