package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.model.User;
import ru.baulina.tm.repository.IUserRepository;
import ru.baulina.tm.util.HashUtil;

import java.util.List;
import java.util.Objects;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    protected IUserRepository userRepository;

    @NotNull
    @Autowired
    protected UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<User> findListUsers() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public User findUser(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        return userRepository.findUser(login, password);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public User findById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        return userRepository.findById(id).get();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final Long id) {
        if (id == null || id < 0) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @Override
    @Transactional
    public void removeUser(@Nullable final User user) {
        if (user == null) return;
        userRepository.delete(user);
    }

    @Nullable
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
        userRepository.save(user); //persist
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
        user.setEmail(email);
        userRepository.save(user); //persist
        return user;
    }

    @Nullable
    @Override
    @Transactional
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @Nullable final User user = new User();
        user.setRole(role);
        user.setLogin(login);
        user.setPasswordHash(Objects.requireNonNull(HashUtil.salt(password)));
        userRepository.save(user); //persist
        return user;
    }

    @Override
    @Transactional
    public void lockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        userRepository.save(user); //merge
    }

    @Override
    @Transactional
    public void unlockUserLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        userRepository.save(user); //merge
    }

    @Override
    @Transactional
    public void changePassword(
            @Nullable final String passwordOld,
            @Nullable final String passwordNew,
            @Nullable final Long userId
    ) {
        final User user = findById(userId);
        if (user == null) throw new AccessDeniedException();
        final String hashOld = HashUtil.salt(passwordOld);
        if (hashOld == null) throw new AccessDeniedException();
        if (!hashOld.equals(user.getPasswordHash())) throw new AccessDeniedException();
        final String hashNew = HashUtil.salt(passwordNew);
        assert hashNew != null;
        user.setPasswordHash(hashNew);
        userRepository.save(user); //merge
    }

    @Override
    @Transactional
    public void changeUser(
            @Nullable final String email,
            @Nullable final String firstName,
            @Nullable final String LastName,
            @Nullable final Long userId
    ) {
        if (userId == null) throw new EmptyUserIdException();
        final User user = findById(userId);
        assert user != null;
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(LastName);
        userRepository.save(user); //merge
    }

}
