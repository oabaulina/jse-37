package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.baulina.tm.api.service.ITaskService;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.TaskNotFoundException;
import ru.baulina.tm.model.Task;
import ru.baulina.tm.model.User;
import ru.baulina.tm.repository.ITaskRepository;

import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    protected ITaskRepository taskRepository;

    @NotNull
    @Autowired
    protected TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final User user,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        taskRepository.save(task); //merge
        return task;
    }
    
    @Nullable
    @Override
    @Transactional
    public Task create(
            @Nullable final User user,
            @Nullable final String name, @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Task task = new Task();
        task.setUser(user);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        taskRepository.clear(userId); //merge
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<Task> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        return taskRepository.findAllByUserId(userId); //findAll
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public List<Task> findListTasks() {
        return taskRepository.findListTasks();
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Task findOneById(
            @Nullable final Long userId, 
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        return taskRepository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public Task findOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    @Transactional
    public void remove(
            @Nullable final Task task
    ) {
        if (task == null) throw new EmptyTaskException();
        taskRepository.save(task); //merge
    }

    @Override
    @Transactional
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable Task task = taskRepository.removeOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = taskRepository.removeOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
    }

    @Override
    @Transactional
    public void updateTaskById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        final Task task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        taskRepository.save(task); //merge
    }

}
