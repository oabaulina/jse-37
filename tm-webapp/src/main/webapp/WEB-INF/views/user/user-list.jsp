<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<jsp:include page="../../include/_header.jsp"/>
<div style="padding-left:16px">
  <h2>USER LIST</h2>
</div>

<div style="padding:10px">
<table width="100%" cellpadding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="100%">LOGIN</th>
        <th width="200" nowrap="nowrap" align="left">EMAIL</th>
        <th width="230" nowrap="nowrap" align="center">FERST NAME</th>
        <th width="230" nowrap="nowrap" align="center">LAST NAME</th>
        <th width="100" nowrap="nowrap" align="center">EDIT</th>
        <th width="100" nowrap="nowrap" align="center">DELETE</th>
    </tr>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                <c:out value="${user.id}" />
            </td>
            <td>
                <c:out value="${user.login}" />
            </td>
            <td>
                <c:out value="${user.email}" />
            </td>
            <td>
                <c:out value="${user.firstName}" />
            </td>
            <td>
                <c:out value="${user.lastName}" />
            </td>
            <td align="center">
                <a href="/user/edit/${user.id}">EDIT</a>
            </td>
            <td align="center">
                <a href="/user/delete/${user.id}">DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>
</div>

<form action="/user/create" style="margin-top: 20px; margin-left: 10px;">
    <button>CREATE USER</button>
</form>

<jsp:include page="../../include/_footer.jsp"/>
