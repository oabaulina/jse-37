<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="${pageContext.request.contextPath}/css/style-menu.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/style-edit.css" rel="stylesheet">
</head>
<body>

<div class="topnav" id="myTopnav">
    <a href="/users" class="active">Users</a>
    <a href="/projects">Projects</a>
    <a href="/tasks">Tasks</a>
    <form action="/exit" method="post">
        <button type="submit">Exit</button>
    </form>
</div>
